#!/bin/sh

# check sudo rights
#user=$(whoami)
#if [ $user != "root" ]; then
#    echo ERROR: should be executed with sudo rights. Current user is $user.
#    exit 1
#fi

# kill existing daemons
ps -Af | grep /home/tigeral/.linux-scripts/input-devices-tweaks/trackpoint.py | sed -E "s/^[^ ]+[ ]+([0-9]+) .*/\1/g" | xargs -n1 kill -9

# find trackpoint device
trackpointDeviceId=$(xinput list --id-only "TPPS/2 IBM TrackPoint")
#trackpointDeviceFile=$(xinput list-props "$trackpointDeviceId" | grep "Device Node" | sed -E 's/[^\"]*\"([^\"]*)\"/\1/g')
trackpointDeviceFile="/dev/input/$(cat /proc/bus/input/devices | grep -E "^[NH]: " | pcregrep -M "Name=\"TPPS/2 IBM TrackPoint\"\n.*" | grep -E "^H: " | sed -E "s/.*(mouse[0-9]).*/\1/g")"

# disable trackpoint as pointer device
xinput set-prop "$trackpointDeviceId" "Device Enabled" 0
chown tigeral /dev/uinput
chown tigeral "${trackpointDeviceFile}"

echo /home/tigeral/.linux-scripts/input-devices-tweaks/trackpoint.py ${trackpointDeviceFile}
sudo -u tigeral /home/tigeral/.linux-scripts/input-devices-tweaks/trackpoint.py ${trackpointDeviceFile} &