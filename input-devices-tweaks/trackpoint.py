#!/usr/bin/env python

import uinput
import sys
import math


def main(trackpointDeviceFile):
    print trackpointDeviceFile + "sdfsdfs"
    pipe = open(trackpointDeviceFile, 'r')
    xScrollMultiplier = 0.3
    yScrollMultiplier = 0.1
    xAcc = 0.0
    yAcc = 0.0

    with uinput.Device([uinput.REL_X, uinput.REL_Y,
                        uinput.BTN_LEFT, uinput.BTN_RIGHT, uinput.REL_WHEEL, uinput.REL_HWHEEL]) as device:
        while 1:
            data = pipe.read(3)
            xByte = ord(data[1])
            yByte = ord(data[2])
            x = xByte if xByte < 128 else -256 + xByte
            y = yByte if yByte < 128 else -256 + yByte
            # print('[{}, {}]'.format(x, y))

            # smart acceleration
            x = x if abs(x) < 50 else x + 2 * math.copysign(abs(x) - 50, x)
            y = y if abs(y) < 50 else y + 2 * math.copysign(abs(y) - 50, y)

            xAcc += x * xScrollMultiplier
            yAcc += y * yScrollMultiplier

            if abs(xAcc) > 1.0:
                device.emit(uinput.REL_HWHEEL, int(xAcc))
                print('x | ' + str(xAcc) + " | " + str(x))
                xAcc -= int(xAcc)
            if abs(yAcc) > 1.0:
                device.emit(uinput.REL_WHEEL, int(yAcc))
                print('y | ' + str(yAcc) + " | " + str(y))
                yAcc -= int(yAcc)


if __name__ == '__main__':
    print sys.argv 
    main(sys.argv[1])
