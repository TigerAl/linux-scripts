#!/bin/sh


# check sudo rights
user=$(whoami)
if [ $user != "root" ]; then
    echo ERROR: should be executed with sudo rights. Current user is $user.
    exit 1
fi

# kill a evrouter daemon if it is already started
echo killall evrouter
killall evrouter

# make a keyboard zoom stick work like a mouse scroll and hinkpad keyboard PrtSc key with Menu key. Require root to change device access rights.
chown tigeral /dev/input/by-id/usb-Microsoft_Natural®_Ergonomic_Keyboard_4000-if01-event-kbd
chown tigeral /dev/input/by-path/platform-i8042-serio-0-event-kbd
runuser tigeral -c 'evrouter -r -c /home/tigeral/.linux-scripts/input-devices-tweaks/evrouter-mapping /dev/input/by-id/usb-Microsoft_Natural®_Ergonomic_Keyboard_4000-if01-event-kbd /dev/input/by-path/platform-i8042-serio-0-event-kbd'
