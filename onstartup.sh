#!/bin/bash

# map keyboard multimedia keys.
if [ -f /home/tigeral/linux-scripts/input-devices-tweaks/xmodmap-mapping ]; then
    runuser -l tigeral -c 'xmodmap /home/tigeral/.linux-scripts/input-devices-tweaks/xmodmap-mapping'
fi

# make a keyboard zoom stick work like a mouse scroll a
/home/tigeral/.linux-scripts/input-devices-tweaks/evrouter-daemon-restart.sh
/home/tigeral/.linux-scripts/input-devices-tweaks/trackpoint-daemon-restart.sh

# setup environment variables