#!/bin/bash
#
# This script used to show/hide some application. Here could be following several cases:
# 1. Application isn't launched -> then the <command> will be executed to start this app.
# 2. Application is launched but minimized to tray (like Mozilla Thunderbird) -> then 
#    the <command> will be executed in order to bring this app to front.
# 3. Application is launched but the app's window is not a currently active window -> 
#    then the focus would be moved to this window.
# 4. Applocation is launched and it's window in focus -> then this window would be 
#    minimized or closed (according to choosen option).


# check if the -h or -c flags passed to args or detect any unknown flag
while : ; do
    case $1 in
      -h | --help | -\?)
        usage
        exit 0
        ;;
      
      -k | --kill)
        killInsteadOfMinimizing=true
        shift
        ;;

      -c | --close)
        closeInsteadOfMinimizing=true
        shift
        ;;

      -class)
        useWindowClassInsteadOfName=true
        shift
        ;;

      --)  # End of all options
        shift
        break
        ;;

      -* | --*)
        echo "Unknown option $1"
        shift
        ;;

      *)  # no more options. Stop while loop
        break
        ;;
    esac
done

function usage {
  echo 
  echo "Usage: $(basename $0) [options] <command> [<window name>]"
  echo "Options:"
  echo " -k | --kill   Kill window instead of minimizing. Also kill all hidden windows."
  echo " -c | --close  Close window with Alt+F4 instead of minimizing."
  echo "      --class  Use window class instead of window name to detect the app window."
  echo " -h | --help   Show this message."
  echo ""
  echo "If <window name> isn't specified then <command> value used instead with the --class option."
}

# Validate <command> param
type "$1" > /dev/null
isInPathResult=$?
if [ -z "$1" ] ; then
  echo "The <command> parameter is missing."
  usage
  exit 1
elif [ $isInPathResult -ne 0 ] && [ ! -e "$1" ] ; then
  echo "The $1 application isn't found."
  exit 1
elif [ $isInPathResult -ne 0 ] && [ -e "$1" ] && [ ! -x "$1" ] ; then
  echo "$1 isn't executable."
  usage
  exit 1
else
  commandLine=$1
fi

# Validate <window name> param
if [ -z "$2" ] ; then
  echo "The <window name> parameter is missing. <command> parameter would be used instead"
  windowName=$commandLine
  useWindowClassInsteadOfName=true
else
  windowName=$2
fi

# Check that only one option -k or -c is active in the same time.
if [ $killInsteadOfMinimizing ] && [ $closeInsteadOfMinimizing ] ; then
  echo "only one option -k or -c could be active in the same time. $killInsteadOfMinimizing"
  usage
  exit 1
fi

# Check if xdotool is available
type "xdotool" > /dev/null
if [ $? -ne 0 ] ; then
  echo "xdotool isn't available. Check if you have rights to use this tool or try to install it."
  echo "If you are using the apt-get package manager then use following command to install xdotool:"
  echo "    sudo apt-get install xdotool"
  exit 1
fi

# Look if window is active
if [ $useWindowClassInsteadOfName ] ; then
  echo "xdotool search --class \"$windowName\" | $useWindowClassInsteadOfName"
  foundWindowIds=$(xdotool search --class "$windowName")
else
  echo "xdotool search --name \"$windowName\""
  foundWindowIds=$(xdotool search --name "$windowName")
fi 
if [ $? -ne 0 ] ; then
  echo "Window with name \"$windowName\" isn't found."
  echo "Execute command \"$commandLine\"."
  $commandLine &
else
  # check if window is active
  activeWindowId=$(xdotool getactivewindow)
  for windowId in $foundWindowIds ; do
    if [ $activeWindowId = $windowId ] ; then
      echo "Window \"$windowName\" is active."
      isActiveWindow=true
      break
    fi
  done
  if [ $isActiveWindow ] ; then
    if [ $killInsteadOfMinimizing ] ; then
      echo "Kill window \"$windowName\"."
      xdotool getactivewindow windowkill > /dev/null
      # kill all hidden windows (look for windows without --desktop option)
      if [ $useWindowClassInsteadOfName ] ; then
        foundWindowIds=$(xdotool search --class "$windowName")
      else
        foundWindowIds=$(xdotool search --name "$windowName")
      fi 
      for windowId in $foundWindowIds ; do
        echo "Kill window \"$windowId\"."
        xdotool windowkill $windowId > /dev/null
      done
    elif [ $closeInsteadOfMinimizing ] ; then
      echo "Close window \"$windowName\" with Alt+F4."
      xdotool getactivewindow key alt+F4 > /dev/null
    else
      echo "Minimize window \"$windowName\"."
      xdotool getactivewindow windowminimize > /dev/null
    fi
  else 
    echo "Application window is not in focus. Bring it to front."
    for windowId in $foundWindowIds ; do
      echo "Activating window $windowId."
      xdotool windowactivate $windowId > /dev/null
      activeWindowId=$(xdotool getactivewindow) 
      if [ $activeWindowId = $windowId ] ; then
        echo "Window was activated sucessfully."
        break
      else 
        echo "Window $windowId couldn't be activated. Trying next windowId."
      fi
    done
  fi
fi
