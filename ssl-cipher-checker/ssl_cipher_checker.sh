#!/usr/bin/env bash

# check if the -s flag passed to args and set the useSuiteNames option
while : ; do
    case $1 in
      -h | --help | -\?)
        usage
        exit 0
        ;;
      
      -s | --suite)
        useSuiteNames=true
        shift
        ;;

      --)  # End of all options
        shift
        break
        ;;

      -* | --*)
        echo "Unknown option $1"
        shift
        ;;

      *)  # no more options. Stop while loop
        break
        ;;
    esac
done

function usage {
  echo 
  echo Usage: $(basename "$0") "[options] <server>:<port> [<cipher names>]"
  echo Options:
  echo " -s | --suite   Use cipher suite names instead of direct cipher names"
  echo " -h | --help    Show this message"
}

# Validate <server>:<port> attribute string
urlchars=']A-Za-z0-9#\.,;\_\~:\/\?@\!\$\&\*\+\-\=\(\)\['"'"
if [ -z "$1" ] ; then
  echo "The <server>:<port> is required argument."
  usage
  exit 1
elif [[ $1 =~ ^[${urlchars}]+:[[:digit:]]+$ ]] ; then
  server=$1
  echo "Server: "$server
else
  echo "Incorrect server name and port format ("$1")"
  usage
  exit 1
fi

# look for cipher names
if [ -z "$2" ] && [ ! $useSuiteNames ] ; then
  # if no -s flag and no cipher names were specified then we should use cipher list from openssl tool
  echo Obtaining cipher list from $(openssl version).
  ciphers=$(openssl ciphers 'ALL:eNULL' | sed -e 's/:/ /g')

elif [ -z "$2" ] && [ $useSuiteNames ] ; then
  # the -s flag was specified but no cipher names were specified. Error.
  echo "The non empty list of cipher names required if the -s option set."
  usage
  exit 1

elif [ $useSuiteNames ] ; then
  # use the cipher suite name values which is specified in args and convert it to cipher names.
  ciphers=()
  for suiteName in ${*:2} ; do
#    echo "suiteName $suiteName"
    ciphers+=`awk '$1=="'$suiteName'" {print $1":"$2" "}' ciphers_list.txt`
  done
#  echo Use the next cipher list: "$ciphers"

else
  # use cipher from args directly, without the "suite name -> cipher name" conversion.
  ciphers="${*:2}"
#  echo Use the next cipher list: "$ciphers"
fi

# test server ssl cipher support with openssl tool
for cipher in ${ciphers} ; do
  if [ ${useSuiteNames} ] ; then
    suiteName=${cipher%%:*}
    cipher=${cipher#*:}
    echo -n Testing $suiteName \($cipher\)...
  else
    echo -n Testing $cipher...
  fi
  result=$(echo -n | openssl s_client -cipher $cipher -connect $server 2>&1)
  if [[ "$result" =~ "BEGIN CERTIFICATE" ]] ; then
    echo YES
  else
    if [[ "$result" =~ ":error:" ]] ; then
      error=$(echo -n $result | cut -d':' -f6)
      echo NO \($error\)
    else
      echo UNKNOWN RESPONSE
      echo $result
    fi
  fi
sleep 1
done
