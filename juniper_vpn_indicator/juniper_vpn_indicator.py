#!/usr/bin/env python

import pygtk
pygtk.require('2.0')
import gtk
import appindicator
import os
import subprocess
import threading
from time import sleep

START_COMMAND = "sudo ~/.juniper_networks/network_connect/ncsvc -h icvpn.icontrol.com -u atarasyuk_ext -p bNopjRflk8J5 -f ~/.juniper_networks/network_connect/ive.crt -r 'Active Directory'"
STOP_COMMAND = "sudo ~/.juniper_networks/network_connect/ncsvc -K"
WATCH_COMMAND = "~/.juniper_networks/network_connect/ncdiag -t | grep 'NC Tunnel Test' | cut -f3 | xargs"
WATCHDOG_CHECK_INTERVAL = 5  # seconds
WATCHDOG_CONNECTING_CHECK_INTERVAL = 0.5  # seconds
FAIL_COUNT_BEFORE_RECONNECT = 3
RECONNECT_COUNT_BEFORE_FINAL_FAIL = 5

BASE_DIR = os.path.dirname(__file__)

class MyGtkIndicator:
    def __init__(self):
        self.indicator = appindicator.Indicator ("example-simple-client", "indicator-messages",
                                                 appindicator.CATEGORY_APPLICATION_STATUS)
        self.indicator.set_status (appindicator.STATUS_ACTIVE)
        self.indicator.set_attention_icon ("indicator-messages-new")
        self.indicator.set_icon(get_resource_path("icon_disconnected.gif"))

        # create a menu
        self.menu = gtk.Menu()
        # status line
        self.statusItem = statusItem = gtk.ImageMenuItem()
        statusItem.set_always_show_image(True)
        statusItem.set_sensitive(False)
        statusItem.show()
        self.menu.append(statusItem)
        # separator
        separatorItem = gtk.SeparatorMenuItem()
        separatorItem.show()
        self.menu.append(separatorItem)
        # connect menu item
        self.connectItem = actionItem = gtk.ImageMenuItem(gtk.STOCK_CONNECT)
        actionItem.set_label("Connect")
        actionItem.connect("activate", self.connect)
        actionItem.show()
        self.menu.append(actionItem)
        # disconnect menu item
        self.disconnectItem = actionItem = gtk.ImageMenuItem(gtk.STOCK_DISCONNECT)
        actionItem.set_label("Disconnect")
        actionItem.connect("activate", self.disconnect)
        actionItem.show()
        self.menu.append(actionItem)
        # reconnect menu item
        self.reconnectItem = actionItem = gtk.ImageMenuItem(gtk.STOCK_REFRESH)
        actionItem.set_label("Reconnect")
        actionItem.connect("activate", self.reconnect)
        actionItem.show()
        self.menu.append(actionItem)
        # separator
        separatorItem = gtk.SeparatorMenuItem()
        separatorItem.show()
        self.menu.append(separatorItem)
        # quit menu item
        self.quitItem = quitItem = gtk.ImageMenuItem(gtk.STOCK_QUIT)
        quitItem.connect("activate", self.quit)
        quitItem.set_always_show_image(True)
        quitItem.show()
        self.menu.append(quitItem)

        # initialize status images
        self.imageConnected = img = gtk.Image()
        img.set_from_stock(gtk.STOCK_CONNECT, gtk.ICON_SIZE_MENU)
        img.show()
        self.imageDisconnected = img = gtk.Image()
        img.set_from_stock(gtk.STOCK_DISCONNECT, gtk.ICON_SIZE_MENU)
        img.show()
        self.imageInProcess = img = gtk.Image()
        img.set_from_stock(gtk.STOCK_REFRESH, gtk.ICON_SIZE_MENU)
        img.show()
        self.imageFailed = img = gtk.Image()
        img.set_from_stock(gtk.STOCK_DIALOG_WARNING, gtk.ICON_SIZE_MENU)
        img.show()

        self.mark_disconnected()
        self.menu.show()
        self.indicator.set_menu(self.menu)

    def mark_connected(self):
        self.indicator.set_icon(get_resource_path("icon_connected.gif"))
        self.statusItem.set_image(self.imageConnected)
        self.statusItem.set_label("Connected")
        self.set_action_items_enabled(True)
        self.connectItem.hide()
        self.disconnectItem.show()
        self.reconnectItem.show()
        self.status = "connected"

    def mark_disconnected(self):
        self.indicator.set_icon(get_resource_path("icon_disconnected.gif"))
        self.statusItem.set_image(self.imageDisconnected)
        self.statusItem.set_label("Disconnected")
        self.set_action_items_enabled(True)
        self.disconnectItem.hide()
        self.connectItem.show()
        self.reconnectItem.show()
        self.status = "disconnected"

    def mark_failed(self):
        self.indicator.set_icon(get_resource_path("icon_disconnected.gif"))
        self.statusItem.set_image(self.imageFailed)
        self.statusItem.set_label("Failed")
        self.set_action_items_enabled(True)
        self.disconnectItem.hide()
        self.connectItem.hide()
        self.reconnectItem.show()
        self.status = "failed"

    def mark_connecting(self):
        self.indicator.set_icon(get_resource_path("icon_disconnected.gif"))
        self.statusItem.set_image(self.imageInProcess)
        self.statusItem.set_label("Connecting...")
        self.set_action_items_enabled(False)
        self.status = "connecting"

    def mark_disconnecting(self):
        self.indicator.set_icon(get_resource_path("icon_connected.gif"))
        self.statusItem.set_image(self.imageInProcess)
        self.statusItem.set_label("Disconnecting...")
        self.set_action_items_enabled(False)
        self.status = "disconnecting"

    def mark_reconnecting(self):
        self.indicator.set_icon(get_resource_path("icon_disconnected.gif"))
        self.statusItem.set_image(self.imageInProcess)
        self.statusItem.set_label("Reconnecting...")
        self.set_action_items_enabled(False)
        self.status = "reconnecting"

    def connect(self, widget=None, data=None):
        self.mark_connecting()
        Starter(self).start()

    def disconnect(self, widget=None, data=None):
        self.mark_disconnecting()
        Stopper(self).start()

    def reconnect(self, widget=None, data=None):
        self.mark_reconnecting()
        Restarter(self).start()

    def quit(self, widget=None, data=None):
        gtk.main_quit()

    def set_action_items_enabled(self, enabled=True):
        self.connectItem.set_sensitive(enabled)
        self.disconnectItem.set_sensitive(enabled)
        self.reconnectItem.set_sensitive(enabled)


class Starter(threading.Thread):
    def __init__(self, app):
        super(Starter, self).__init__()
        self.app = app

    def run(self):
        subprocess.Popen(START_COMMAND, shell=True).communicate()
        # vpn shell process closed
        if self.app.status in ["connecting", "connected"]:
            self.app.mark_failed()
        else:
            self.app.mark_disconnected()


class Stopper(threading.Thread):
    def __init__(self, app):
        super(Stopper, self).__init__()
        self.app = app

    def run(self):
        subprocess.Popen(STOP_COMMAND, shell=True).communicate()
        # all vpn processes are closed successfully
        self.app.mark_disconnected()


class Restarter(threading.Thread):
    def __init__(self, app):
        super(Restarter, self).__init__()
        self.app = app

    def run(self):
        subprocess.Popen(STOP_COMMAND, shell=True).communicate()
        self.app.mark_reconnecting()
        subprocess.Popen(START_COMMAND, shell=True).communicate()
        # vpn shell process closed
        if self.app.status in ["reconnecting", "connecting", "connected"]:
            self.app.mark_failed()
        else:
            self.app.mark_disconnected()


class WatchDog(threading.Thread):
    def __init__(self, app):
        super(WatchDog, self).__init__()
        self.app = app

    def run(self):
        sleep(0.1)
        failuresCount = 0
        reconnectCount = 0
        while True:
            if self.app.status == "connected":
                result = subprocess.check_output(WATCH_COMMAND, shell=True)
                if result.startswith("Not established"):
                    self.app.mark_failed()
                    failuresCount = 1
                failuresCount = 0
                reconnectCount = 0
            elif self.app.status in ["connecting", "reconnecting"]:
                result = subprocess.check_output(WATCH_COMMAND, shell=True)
                if result.startswith("Established"):
                    self.app.mark_connected()
                else:
                    sleep(WATCHDOG_CONNECTING_CHECK_INTERVAL)
                    continue
            if self.app.status == "failed":
                failuresCount += 1
                if failuresCount >= FAIL_COUNT_BEFORE_RECONNECT:
                    if reconnectCount < RECONNECT_COUNT_BEFORE_FINAL_FAIL:
                        reconnectCount += 1
                        self.app.reconnect()
            if gtk.main_level() == 0:
                subprocess.Popen(STOP_COMMAND, shell=True).communicate()
                exit()
            sleep(WATCHDOG_CHECK_INTERVAL)


def get_resource_path(rel_path):
    return os.path.abspath(os.path.join(BASE_DIR, rel_path))


if __name__ == "__main__":
    # subprocess.Popen("/home/tigeral/.juniper_networks/network_connect/msjnc", shell=True).communicate()
    app = MyGtkIndicator()
    gtk.threads_init()
    WatchDog(app).start()
    app.connect()
    gtk.main()